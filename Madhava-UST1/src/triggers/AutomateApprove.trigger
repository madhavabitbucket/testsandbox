trigger AutomateApprove on Opportunity(After insert, After update)

{
    for (Integer i = 0; i < Trigger.new.size(); i++)
    {
     try
    {
        //Insure that previous value not equal to current, else if there is any field update on approval process action
        //there will be recurrence of the trigger.
        if(Trigger.new[i].Next_Step__c == 'Submit' && Trigger.old[i].Next_Step__c != 'Submit')
        {
           submitForApproval(Trigger.new[i]);
        }
     /*   else if(Trigger.new[i].Next_Step__c == 'Approve' && Trigger.old[i].Next_Step__c != 'Approve')
       {
             approveRecord(Trigger.new[i]);
        }
       else if(Trigger.new[i].Next_Step__c == 'Reject' && Trigger.old[i].Next_Step__c != 'Reject')
        {
             rejectRecord(Trigger.new[i]);
        }  */
     }catch(Exception e)
     {
         Trigger.new[i].addError(e.getMessage());
     }
    }
    // This method will submit the opportunity automatically
    public void submitForApproval(Opportunity opp)
    {
        // Create an approval request for the Opportunity
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval automatically using Trigger');
        req1.setObjectId(opp.id);
        req1.setNextApproverIds(new Id[] {opp.Next_Approver__c});
        // Submit the approval request for the Opportunity
        Approval.ProcessResult result = Approval.process(req1);
      
    }
    /*
   //Get ProcessInstanceWorkItemId using SOQL
    public Id getWorkItemId(Id targetObjectId)
    {
        Id retVal = null;
        for(ProcessInstanceWorkitem workItem  : [Select p.Id from ProcessInstanceWorkitem p
            where p.ProcessInstance.TargetObjectId =: targetObjectId])
        {
          retVal  =  workItem.Id;
        }
        return retVal;
    }
    // This method will Approve the opportunity
    public void approveRecord(Opportunity opp)
    {
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setComments('Approving request using Trigger');
        req.setAction('Approve');
       req.setNextApproverIds(new Id[] {opp.Next_Approver__c});
        Id workItemId = getWorkItemId(opp.id);
        //opp.addError(workItemId);
        if(workItemId == null)
        {
            opp.addError('Error Occured in Trigger');
           //opp.addError(workItemId);
        }
        else
        {
          req.setWorkitemId(workItemId);
            // Submit the request for approval
            Approval.ProcessResult result =  Approval.process(req);
        }
    }
   // This method will Reject the opportunity
    public void rejectRecord(Opportunity opp)
    {
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
       req.setComments('Rejected request using Trigger');
        req.setAction('Reject');
        //req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        Id workItemId = getWorkItemId(opp.id);
        //opp.addError(workItemId);
        if(workItemId == null)
        {
            opp.addError('Error Occured in Trigger');
            //opp.addError(workItemId);
        }
        else
        {
            req.setWorkitemId(workItemId);
           // Submit the request for approval
            Approval.ProcessResult result =  Approval.process(req);
        }
  }
  */
}