@isTest
global class MockHttpResponseGenerator1 implements HttpCalloutMock {
    // Implement respond() interface method
    
    global HTTPResponse respond(HTTPRequest req) {
        System.assertEquals('http://cheenath.com/tutorial/sample1/build.xml', req.getEndpoint());
        System.assertEquals('get', req.getMethod());
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml');
           res.setBody('<env:Envelope><env:Header></env:Header><env:Body><ghr:return><ghr:array><ghr:item>'+
         '<ghr:account>PT015AC</ghr:account><ghr:key>PT015AC</ghr:key><ghr:lmMon>201205</ghr:lmMon><ghr:dspMon>May 2012</ghr:dspMon>'+
         '<ghr:total>0.00</ghr:total><ghr:totTrade>0.00</ghr:totTrade><ghr:status/></ghr:item>'+
         '</ghr:array></ghr:return></env:Body></env:Envelope>');
            res.setStatusCode(200);
        return res;
    }
}