public class Activity_Planning {

    public String strActivityID; // for storing current page Activity id
    public String strPlanningID; // for storing current Planning data record id
    /*Start INC000006044872*/
    public String strEventUniqueNumber; // for storing unique event id for parent event.
    /*End INC000006044872*/
    Boolean UpdateActivityflag; // for storing flag if Planninf data has changed
    Public Boolean ShowPageFlag{get;set;} // for storing flag if Page needs to be displayed    
    
    String Agenda = '';
    String Desired_Outcome = '';
    String Confirmation_Letter = '';
    String InternalpostcallNotes = '';
    
    public Boolean activityrelatedflag{get;set;} // for storing flag if Activity is related to any entity.
    
    public Boolean createPlanningflag; // for storing flag if Planning Data is being created
    Public Event eveObj{get;set;}
    Public Planning_Data__c planningObj{get;set;}
    Contact ConObj = New Contact();
    Lead LeadObj = New Lead();
    
    Public String AttachmentLoadFrom{get;set;}
    
    public Activity_Attachment__c objActivityAttachment{get;set;}//Activity Attachment Object
    public Attachment objAttachment{get;set;}
    public Activity_Attachment__c objActivityAttachmentShow{get;set;}
    public boolean activityUpsertSuccessFlag{get;set;}
    public boolean displayAttachmentFlag{get;set;}
    public Boolean bUploadMsg{get;set;}//used For rerendered if true show the attachment details :name,size,type  on UI.
    public String strFileSize{get;set;}//used to store file size

    // For Showing Attachments
    public List<activityAttachWrapper> activityAttachWrapperList{get;set;}//wrapper class list
    List<Activity_Attachment__c>  activityattList=new List<Activity_Attachment__c>();//activity attachment list
    Set<ID> activityattIdSet= new Set<ID>();//activity attachment id set
    public List<Attachment>  attList=new List<Attachment>();//attachment list

    public boolean blRelatedListShow{get;set;}//if no records are present the show "No records to display"
    public boolean showAddAttachment{get;set;}//if Planning Data is not saved then dont show Add Attachment Button
    public String strGetVal{get;set;}//used in window.open() javascript method 
    public String stractivityAttachId{get;set;}//Activity attachment id 
    public String alertMessage {get;set;}//js alert message
    public boolean disableDeleteLink {get;set;} // preventing user to delete record


    public Activity_Planning(ApexPages.StandardController controller) {
    
        createPlanningflag = False;
        UpdateActivityflag = False;
        activityrelatedflag = False;
        ShowPageFlag = False;
        bUploadMsg=FALSE;
        
        eveObj = New Event();
        objActivityAttachment = New Activity_Attachment__c();
        
        if(ApexPages.currentPage().getParameters().get('actid') != null)
            strActivityID = ApexPages.currentPage().getParameters().get('actid');

        if(ApexPages.currentPage().getParameters().get('id') != null)
            strPlanningID = ApexPages.currentPage().getParameters().get('id');

        /*Start INC000006044872*/
        if(ApexPages.currentPage().getParameters().get('uniqid') != null)
            strEventUniqueNumber = ApexPages.currentPage().getParameters().get('uniqid');
        /*End INC000006044872*/
            
        if(Schema.sObjectType.Activity_Attachment__c.isDeletable()){
            disableDeleteLink=true;
        }

        if( strActivityID != '' )
        {
            Try{
            /*Start INC000006044872 - Modified where clause*/

               eveObj =  [ Select id,Description,StartDateTime,EndDateTime,DurationInMinutes,Subject,Location,Type,Legacy_ID__c,
                            WhoId,WhatId,Who.Name,What.Name,Who.Type,What.Type,Who.Email,Who.Phone, OwnerId, AccountId,
                            IsAllDayEvent,IsPrivate,ShowAs,ActivityDateTime,Accompanied_by_Sales_Manager__c,Executive_Selling__c,
                            Internal_Notes__c,Pre_Call_Notes__c,Post_call_Notes__c,Activity_Unique_No1__c,Planning_Data_ID__c from Event where Activity_Unique_No1__c = :strEventUniqueNumber and IsChild = false Limit 1];
            /*End INC000006044872*/


                if( eveObj != NULL && eveObj.Id != NULL )
                    ShowPageFlag = True;
            }
            Catch(Exception e){}
    
            if( !ShowPageFlag )
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Corresponding Event is Deleted.');
                ApexPages.addMessage(myMsg);        
            }
            else if( ShowPageFlag )
            {
                if( eveObj.WhatId != NULL )
                    activityrelatedflag = True;
                    
                Try{
                    //planningObj = [ Select id,Agenda__c,Desired_Outcome__c,Confirmation_Letter__c,Internal_post_call_Notes__c from Planning_Data__c where Activity_ID__c = :eveObj.Id Order by Createddate desc limit 1 ];            
                    /*Start INC000006044872 - Modified where clause*/
                    planningObj = [ Select id,Agenda__c,Desired_Outcome__c,Confirmation_Letter__c,Internal_post_call_Notes__c from Planning_Data__c where id = :eveObj.Planning_Data_ID__c];                    
                    /*End INC000006044872*/

                    if( planningObj != NULL )
                        showAddAttachment = True;
                }Catch(Exception e){}

                if( planningObj == NULL )
                {                                    
                    createPlanningflag = True;
                    
                    planningObj = New Planning_Data__c();
                    planningObj.Activity_ID__c = eveObj.Id ;
                    planningObj.Activity_Legacy_ID__c = eveObj.Legacy_ID__c;
                    planningObj.Activity_Type__c = eveObj.Type;   
                    planningObj.Activity_Location__c = eveObj.Location;
                    planningObj.Activity_Who_ID__c = eveObj.WhoId ;
                    planningObj.Activity_Who_Name__c = eveObj.Who.Name ;
                    planningObj.Activity_What_ID__c = eveObj.WhatId ;
                    planningObj.Activity_Related_To_Name__c = eveObj.What.Name ;
                    planningObj.Activity_Who_ID_Type__c =  eveObj.Who.Type ;
                    planningObj.Activity_What_ID_Type__c = eveObj.What.Type ;
                    planningObj.Activity_Start_Date__c = eveObj.StartDateTime ;
                    planningObj.Activity_End_Date__c = eveObj.EndDateTime ;
                    planningObj.Activity_Duration__c = eveObj.DurationInMinutes ;  
                    Try{
                        
                        if( eveObj.Who.Type == 'Contact' )
                        {
                            ConObj = [ Select Id,Name,Email,Phone From Contact where id = :eveObj.WhoId ];
    
                            planningObj.Activity_Email__c = ConObj.Email ;                
                            planningObj.Activity_Phone__c = ConObj.Phone ;                                                                       
                        }
                        if( eveObj.Who.Type == 'Lead' )
                        {
                            LeadObj = [ Select Id,Name,Email,Phone From Lead where id = :eveObj.WhoId ];    
              
                            planningObj.Activity_Email__c = LeadObj.Email ;                
                            planningObj.Activity_Phone__c = LeadObj.Phone ;                                               
                        }
        
                    }Catch(Exception e){}
        
                    planningObj.Accompanied_by_Sales_Manager__c = eveObj.Accompanied_by_Sales_Manager__c;                
                    planningObj.Executive_Selling__c = eveObj.Executive_Selling__c;                
                    planningObj.Associated_Account__c = eveObj.AccountId; 
                    planningObj.Ownerid = eveObj.OwnerId;
                    planningObj.Activity_Assigned_To__c = eveObj.OwnerId;                
                    
                    planningObj.Activity_AllDayEvent__c = eveObj.IsAllDayEvent;                
                    planningObj.Activity_Private__c = eveObj.IsPrivate;                
                    planningObj.Activity_Show_Time_As__c = eveObj.ShowAs;                
                    planningObj.Activity_Time__c = eveObj.ActivityDateTime;   
                    planningObj.Activity_Subject__c = eveObj.Subject;             
    
                    planningObj.Agenda__c = eveObj.Description ;            
    
                }
              
                if( planningObj.Agenda__c != NULL ) 
                    Agenda = planningObj.Agenda__c;
                if( planningObj.Desired_Outcome__c != NULL )
                    Desired_Outcome = planningObj.Desired_Outcome__c;
                if( planningObj.Confirmation_Letter__c != NULL )
                    Confirmation_Letter = planningObj.Confirmation_Letter__c;
                if( planningObj.Internal_post_call_Notes__c != NULL )
                    InternalpostcallNotes = planningObj.Internal_post_call_Notes__c;
    
        
                blRelatedListShow = False;
                activityAttachWrapperList = new List<activityAttachWrapper>();
        
                if( planningObj != NULL && planningObj.ID != NULL )
                {
                    //fetch out all Activity attachment related to Activity id
                    for(Activity_Attachment__c actatt : [Select Id, Type__c From Activity_Attachment__c where Planning_Data__c = :planningObj.Id order by CreatedDate desc])
                    {
                        //set of Activity attachment
                        activityattList.add(actatt);
                        //set of Activity attachment id
                        activityattIdSet.add(actatt.id);
                    }
                        
                    //find out all attachment details for qute attch id which is mentioned above in descending order
                    attList=[Select Id, ParentId, Name, CreatedDate, CreatedById From Attachment where parentId IN : activityattIdSet order by CreatedDate desc]; 
                    
                    //iterate through all Activity attachment list:  quoattList
                    for(Activity_Attachment__c actatt : activityattList)
                    {
                        //iterate through all attachment list: attList
                        for(Attachment attFinal : attList)
                            {
                                //doing mapping which attachment is related to Activity attachment
                                if(actatt.id == attFinal.parentId)
                                {
                                    //passing attachment and Activity attachment object to wrapper class
                                    activityAttachWrapperList.add(new activityAttachWrapper(actatt ,attFinal));
                                }
                            }
                    }
                    
                    if(activityAttachWrapperList.size()>0)
                    {
                        blRelatedListShow=TRUE;
                    }           
                } 
            }
        }
    }   
     
    //when user click on Back to Activity Link
    public pageReference BackToActivityClick(){
        PageReference ActivityPage = new PageReference('/'+ strActivityID);
        ActivityPage.setRedirect(true);
        return ActivityPage;
    }    

    //call when "Del" link is called from VF page
    public void delAttachement()
    {
        try
        {
            strGetVal='';
            //initialize to null
            Activity_Attachment__c tobeDeleted = null;      
            //use Activity attachment list related to Activity and finding out which Activity attachment need to be deleted

            for(Activity_Attachment__c q : activityattList)
            {
                if(q.id == stractivityAttachId)
                {
                    tobeDeleted = q;
                    break;
                }
            }
            //delete the Activity attachment which is select
            delete tobeDeleted;          
    
            if( ApexPages.currentPage().getParameters().get('actid') !=null && ApexPages.currentPage().getParameters().get('id') !=null )    
            {
                //if successfully deleted then show this alert message on VF page
                alertMessage='Successfully deleted';
                //used in windoe.open() method in VF
                strGetVal ='/apex/Activity_Planning?id='+strPlanningID+'&actid='+ strActivityID+'&uniqid='+strEventUniqueNumber;
            }
        }
        catch(Exception e)
        {
            system.debug('Exception :'+e);
            //if deletion fail then show this alert message on VF page
            alertMessage = 'An error occurred -- ' + e.getMessage() + 'Please contact administrator';
        }
    }
    
    // return null; 
    //call "View" link is click on VF page
    public PageReference viewAttachement()
    {
        strGetVal='';        
        //fetch the attachment id which need to view
        ID attId=System.currentPageReference().getParameters().get('attachid');
        //Url to redirect to file
        strGetVal = '/servlet/servlet.FileDownload?file='+attId;
        PageReference page = new PageReference(strGetVal); 
        page.setRedirect(true);
        return page; 
    }       
    
    //Save And Close button action
    public PageReference SaveAndCloseAction(){
        System.Savepoint sp ;
        sp = Database.setSavepoint();
        
        if( ShowPageFlag )        
        {
            try
        {
        
                if( eveObj != NULL && eveObj.Id != NULL  )
                {
                    upsert planningObj;
                           

                    strPlanningID = planningObj.Id;
                    
                    activityUpsertSuccessFlag = True;                    
            
                    if( createPlanningflag )
                    {
                        UpdateActivityflag = true;
                        eveObj.Planning_Data_ID__c = planningObj.Id;
                    }
                        
        
                    if( planningObj.Agenda__c != Agenda )
                    {
                        UpdateActivityflag = true;
                        eveObj.Description = planningObj.Agenda__c ;
                    }
                    if( planningObj.Desired_Outcome__c != Desired_Outcome )
                    {
                        UpdateActivityflag = true;
                        if( planningObj.Desired_Outcome__c != '' )
                            eveObj.Pre_Call_Notes__c = planningObj.Desired_Outcome__c.Left(243) + '...(Cont\'d.)';
                        else
                            eveObj.Pre_Call_Notes__c = '';
                    }
                    if( planningObj.Confirmation_Letter__c != Confirmation_Letter )
                    {
                        UpdateActivityflag = true;
                        if( planningObj.Confirmation_Letter__c != '' )
                            eveObj.Post_call_Notes__c = planningObj.Confirmation_Letter__c.Left(243) + '...(Cont\'d.)';
                        else
                            eveObj.Post_call_Notes__c = '';
                    }
                    if( planningObj.Internal_post_call_Notes__c != InternalpostcallNotes )
                    {
                        UpdateActivityflag = true;
                        if( planningObj.Internal_post_call_Notes__c != '' )
                            eveObj.Internal_Notes__c = planningObj.Internal_post_call_Notes__c.Left(243) + '...(Cont\'d.)';
                        else
                            eveObj.Internal_Notes__c = '';                
                    }
                    if( UpdateActivityflag )
                    {
                        Try{
                            Update eveObj;
                        }Catch(Exception e){
                            if(e.GetMessage().Contains('ENTITY_IS_DELETED'))
                            {
                                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Corresponding Event is Deleted.');
                                ApexPages.addMessage(myMsg); 
                                Database.Rollback(sp);
                                ShowPageFlag = False;
                                return null;                               
                            }       
                        }             
                    }
                    
                    PageReference ActivityPage = new PageReference('/' +strActivityID );
                    ActivityPage.setRedirect(true);
                    return ActivityPage; 
    
                }                
            }
            Catch (Exception e){
    
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getmessage() );
                ApexPages.addMessage(myMsg);                                
                Database.Rollback(sp);
                return null;
            }
        }                 
        return null;         
    }
    
    //Save & Attachment Action
    public Pagereference SaveAttachmentAction(){

        SaveAndCloseAction();
        
        if(activityUpsertSuccessFlag)
            AddAttachmentAction();
        
        return null;
    }

    //Save & Attachment Action
    public Pagereference AddAttachmentAction(){
        
        displayAttachmentFlag=true;
        
        return null;
    }

    //Send Email functionality
    public Pagereference SendEmailAction(){
        SaveAndCloseAction();
        
        EmailTemplate tempObj = [ Select id from EmailTemplate where Developername = 'Confirmation_Letter' ];

        PageReference ActivityPage = new PageReference('/_ui/core/email/author/EmailAuthor?p2_lkid='+eveObj.WhoId+'&p3_lkid='+planningObj.Id+'&template_id='+tempObj.Id+'&retURL=%2F'+eveObj.Id);
        ActivityPage.setRedirect(true);
        return ActivityPage; 
    }

    // string representation if a file's size, such as 2 KB, 4.1 MB, etc 
    public String fileSize(integer value){
        if (Value < 1024)
            return string.valueof(Value) + ' Bytes';
        else
            if (Value >= 1024 && Value < (1024*1024)){
            //KB
                Decimal kb = Decimal.valueOf(Value);
                kb = kb.divide(1024,2);
                return string.valueof(kb) + ' KB';
            }
            else
                if (Value >= (1024*1024) && Value < (1024*1024*1024)){
                //MB
                    Decimal mb = Decimal.valueOf(Value);
                    mb = mb.divide((1024*1024),2);
                    return string.valueof(mb) + ' MB';
                }
                else 
                return null;
    }

    //Create attachemnet object
    public Attachment attachment{
        get{
            if (attachment == null)
                attachment = new Attachment();
            return attachment;
        }
        set;
    }

    
    //Call when click Attach File Button from VF Page ,used to save the attachment in attachment object and Type(custom field) into Activity Attcahment Object.
    public PageReference upload(){
        
        System.SavePoint lOrignalState =  database.setSavepoint();
        //assign Activity ID to Activity attachment object
        objActivityAttachment.Planning_Data__c = planningObj.Id;
    
        //insert Activity attachment object    
        insert objActivityAttachment;
    
        attachment.OwnerId = UserInfo.getUserId();
        attachment.ParentId = objActivityAttachment.id; // the record the file is attached to
        attachment.IsPrivate = false;
        
        try 
        {
            //insert attachment object
            insert attachment;
            //Fetch the attachment object fields
            objAttachment=[SELECT BodyLength,Name FROM Attachment where id=:attachment.id];
            //Call filesize method so that able to find size in Bytes ,KB and MB and then used this strFileSize variable to show in UI after attachment uploaded successfully.
            strFileSize=fileSize(objAttachment.BodyLength);
            //used this objActivityAttachmentShow variable to show Type of attachment in UI after attachment uploaded successfully.
            objActivityAttachmentShow = [select Type__c FROM Activity_Attachment__c where id=:objActivityAttachment.id];
            bUploadMsg=TRUE;
        }
        catch (DMLException e){

            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
            //Don't show attachment details if this variable is  false or error has occured so attachment is not uploaded
            bUploadMsg=FALSE;
            database.rollback(lOrignalState);
            return null;
        } 
        finally{
            //create new attachment object
            attachment = new Attachment();
            //create new Activity attachment object                  
            objActivityAttachment = new Activity_Attachment__c();
        }        
        return null;
    }
    
    //when user click on Done button after savind attachment
    public pageReference BackToPlanningData(){
    
        PageReference PlanningPage;

        if( strPlanningID != '' )
            PlanningPage = new PageReference('/apex/Activity_Planning?id='+strPlanningID+'&actid='+ strActivityID+'&uniqid='+strEventUniqueNumber);
        else
            PlanningPage = new PageReference('/apex/Activity_Planning?actid='+ strActivityID);
            
        PlanningPage.setRedirect(true);
        return PlanningPage;
    }    

    //Wrraper class contain attachment and activity attachment object
    public class activityAttachWrapper
    {
        public Activity_Attachment__c activityAttObj{get; set;}
        public Attachment attObj{get; set;}        
        //constructor of wrapper class
        public activityAttachWrapper(Activity_Attachment__c actatt,Attachment attach)
        {
            activityAttObj = actatt;
            attObj = attach;
        }
    }   
    
}